#include "main.h"

SDL_Window* pWindow;  
SDL_Renderer* pRenderer; 
bool mustStop = false; 
Input in;
Sprite player = { 160, 470, 250, 0, 1, 0, 0.0f, false, NULL }; 
Sprite enemy = { 520, 392, 80, 0, 1, 0, 0.0f, false, NULL }; 
Sprite * platforms[MAX_PLATFORM_NUMBER]; 
SDL_Texture * backgroundImg = NULL, * screenBuffer = NULL;  
SDL_Rect bgRect; 
int score = 0; 
bool mayJump = false; 
int worldYGravity = 1000; 
bool isAlive = true; 
bool enemyDead = false; 
char msgToDisplay[80]; 

int main(int argc, char** argv) {
	// FILE* fichier = NULL;
	// int caractereActuel = 0;
	// fichier = fopen("data/level2_map.json", "r");
	// if (fichier != NULL) { // Boucle de lecture des caractères un à un
	// 	do {
	// 		caractereActuel = fgetc(fichier); // On lit le caractère
	// 		printf("%c", caractereActuel); // On l'affiche
	// 	} while (caractereActuel != EOF); // On continue tant que fgetc n'a pas retourné EOF (fin de fichier)
	// 	fclose(fichier);
	// }

	try {
		run(); 
	} catch (sdl_init_failed i) {
		fprintf(stderr,"Fail to initialize SDL (%s)\n",SDL_GetError()); return -1;
	} catch (sdl_window_creation_failed w) {
		fprintf(stderr,"Fail to create window: %s\n",SDL_GetError()); return -1;
	} catch (sdl_renderer_creation_failed r) {
		fprintf(stderr,"Fail to create renderer (%s)\n",SDL_GetError()); return -1;
	} catch (sdl_bitmap_loading_failed b) {
		fprintf(stderr,"Fail to load bitmap in surface (%s)\n",SDL_GetError()); return -1;
	} catch (sdl_texture_creation_failed t) {
		fprintf(stderr,"Fail to create texture from surface (%s)\n",SDL_GetError()); return -1;
	} 
}	

// Run demo using architecture: load(), update(), draw() 
void run() {
	int lastExecutionTimeInMs = 0; 
	bool bufferReadyToDisplay = false;	// do not display screen the first time before having drawn it.  
	init_SDL();
	screenBuffer = SDL_CreateTexture(pRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, WINDOW_WIDTH, WINDOW_HEIGHT);
	memset(&in,0,sizeof(in));
	#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
		strcpy_s(msgToDisplay, "");
	#else
		strcpy(msgToDisplay, "");
	#endif
	load(); 
	while(!mustStop) {
		// SDL_Delay(500); 
		if((SDL_GetTicks() - lastExecutionTimeInMs)>MIN_FRAME_PERIOD_IN_MS) { // display screen with a defined refresh period  
			// cout<<"run: SDL_GetTicks() - lastExecutionTime = "<<SDL_GetTicks() - lastExecutionTime<<"\n"; 
			if(bufferReadyToDisplay) SDL_RenderPresent(pRenderer); 
			UpdateEvents(&in);
			update((float)(SDL_GetTicks() - lastExecutionTimeInMs)/1000); 
			SDL_SetRenderTarget(pRenderer, screenBuffer);	// set render target to buffer texture 
			SDL_RenderClear(pRenderer);
			draw((float)(SDL_GetTicks() - lastExecutionTimeInMs)/1000); 
			lastExecutionTimeInMs = SDL_GetTicks();
			SDL_SetRenderTarget(pRenderer, NULL);	// reset default render target
			SDL_RenderCopy(pRenderer, screenBuffer, NULL, NULL); 
			bufferReadyToDisplay = true; 
		}
	}
}

void load() {
	// load background image
	#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
		backgroundImg = newImage("../data/bg.png");
	#else
		backgroundImg = newImage("data/bg.png");
	#endif	
	bgRect.x = 0; bgRect.y = 7; bgRect.w = 800; bgRect.h = 600;
	// load player sprite 
	#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
		player.img = newImage("../data/sprite-sheet-animations.png");
	#else
		player.img = newImage("data/sprite-sheet-animations.png");
	#endif	
	player.frames[0].x = 0; player.frames[0].y = 66; player.frames[0].w = 80; player.frames[0].h = 127;
	player.frames[1].x = 266; player.frames[1].y = 296; player.frames[1].w = 82; player.frames[1].h = 129; 
	player.frames[2].x = 354; player.frames[2].y = 296; player.frames[2].w = 79; player.frames[2].h = 128; 
	player.frames[3].x = 437; player.frames[3].y = 296; player.frames[3].w = 89; player.frames[3].h = 129; 
	player.frames[4].x = 526; player.frames[4].y = 296; player.frames[4].w = 75; player.frames[4].h = 129; 
	player.frames[5].x = 607; player.frames[5].y = 296; player.frames[5].w = 76; player.frames[5].h = 129; 
	player.frames[6].x = 692; player.frames[6].y = 296; player.frames[6].w = 72; player.frames[6].h = 127; 
	player.anims[0] = (Animation*)malloc(sizeof(Animation));
	player.anims[0]->frameNumber = 6; 
	player.anims[0]->periodInSec = 1.0f; 
	player.anims[0]->sequence[0] = 1; player.anims[0]->sequence[1] = 2; player.anims[0]->sequence[2] = 3; player.anims[0]->sequence[3] = 4; 
	player.anims[0]->sequence[4] = 5, player.anims[0]->sequence[5] = 6; 
	// load enemy sprite
	#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
		enemy.img = newImage("../data/slime.png");
	#else
		enemy.img = newImage("data/slime.png");
	#endif	
	enemy.frames[0].x = 0; enemy.frames[0].y = 0; enemy.frames[0].w = 40; enemy.frames[0].h = 22;
	// load platforms 
	platforms[0] = (Sprite*)malloc(sizeof(Sprite)); 
	platforms[0]->x = 400; platforms[0]->y = 545; platforms[0]->frameToDisplay = 0; 
	#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
		platforms[0]->img = newImage("../data/ground.png");
	#else
		platforms[0]->img = newImage("data/ground.png");
	#endif	
	platforms[0]->frames[0].x = 0; platforms[0]->frames[0].y = 0; platforms[0]->frames[0].w = 800; platforms[0]->frames[0].h = 30;
	for (int i = 1; i < 11; i++) {
		platforms[i] = (Sprite*)malloc(sizeof(Sprite)); 
		platforms[i]->x = 225 + 35 * i; platforms[i]->y = 420; platforms[i]->frameToDisplay = 0; 
		#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
			platforms[i]->img = newImage("../data/tiles_spritesheet.png");
		#else
			platforms[i]->img = newImage("data/tiles_spritesheet.png");
		#endif
		if(i == 0) { platforms[i]->frames[0].x = 252; platforms[i]->frames[0].y = 324; platforms[i]->frames[0].w = 35; platforms[i]->frames[0].h = 35; } 
		else if (i == 10) { platforms[i]->frames[0].x = 252; platforms[i]->frames[0].y = 252; platforms[i]->frames[0].w = 35; platforms[i]->frames[0].h = 35; }
		else { platforms[i]->frames[0].x = 252; platforms[i]->frames[0].y = 288; platforms[i]->frames[0].w = 35; platforms[i]->frames[0].h = 35; }
	}
}

void init_SDL() {
	if (SDL_Init(SDL_INIT_VIDEO) == 0) { // Create window 
		pWindow = SDL_CreateWindow("My sprite SDL",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		WINDOW_WIDTH, WINDOW_HEIGHT,
		SDL_WINDOW_SHOWN);
		if(pWindow) { // Create renderer 
			pRenderer = SDL_CreateRenderer(pWindow,-1,SDL_RENDERER_ACCELERATED);
			if(!pRenderer) throw sdl_renderer_creation_failed();  
		} else throw sdl_window_creation_failed(); 
	} else throw sdl_init_failed(); 
}

SDL_Texture* newImage(const char* relative_path_and_filename) {
	SDL_Texture* outputTexture = NULL; 
	SDL_Surface* pSurface = IMG_Load(relative_path_and_filename);
	if(pSurface) { // Create texture from surface 	
		outputTexture = SDL_CreateTextureFromSurface(pRenderer,pSurface); 
		if (!outputTexture) throw sdl_texture_creation_failed(); 
		SDL_FreeSurface(pSurface);
	} else throw sdl_bitmap_loading_failed(); 
	return outputTexture; 
}

void UpdateEvents(Input* in) {
	SDL_Event event;
	while(SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_KEYDOWN:
			in->key[SDL_GetScancodeFromKey(event.key.keysym.sym)]=1;
			// cout<<"UpdateEvents: SDL_GetScancodeFromKey(event.key.keysym.sym) = "<<SDL_GetScancodeFromKey(event.key.keysym.sym)<<"\n";
			break;
		case SDL_KEYUP:
			in->key[SDL_GetScancodeFromKey(event.key.keysym.sym)]=0;
			break;
		case SDL_MOUSEMOTION:
			in->mousex=event.motion.x;
			in->mousey=event.motion.y;
			in->mousexrel=event.motion.xrel;
			in->mouseyrel=event.motion.yrel;
			break;
		case SDL_MOUSEBUTTONDOWN:
			in->mousebuttons[event.button.button]=1;
			break;
		case SDL_MOUSEBUTTONUP:
			in->mousebuttons[event.button.button]=0;
			break;
		case SDL_QUIT:
			in->quit = 1;
			break;
		default:
			break;
		}
	}
}

void drawImage(SDL_Texture* imageTexture, SDL_Rect* source, int imagePosX, int imagePosY, int imageRotation = 0, int imageScaleX = 1, int imageScaleY = 1) {
	SDL_Rect dest; 
	if(source == NULL) { SDL_QueryTexture(imageTexture, 0, NULL, &dest.w, &dest.h); dest.x = imagePosX - dest.w / 2; dest.y = imagePosY - dest.h /2; } 
	else { dest.x = imagePosX - source->w/2; dest.y = imagePosY - source->h/2; dest.w = source->w; dest.h = source->h; }
	// cout<<"drawImage: dest.w  = "<<dest.w<<", dest.h  = "<<dest.h<<", dest.x = "<<dest.x<<", dest.y = "<<dest.y<<"\n";
	if(imageScaleX == -1) SDL_RenderCopyEx(pRenderer, imageTexture, source, &dest, 0, NULL, SDL_FLIP_HORIZONTAL); // texture horizontal flip before rendering 
	else SDL_RenderCopy(pRenderer, imageTexture, source, &dest); 
}

bool checkCollision(Sprite * s1, Sprite * s2) {
	bool outputValue = false; 
	int x1 = s1->x - s1->frames[s1->frameToDisplay].w/2; int y1 = s1->y - s1->frames[s1->frameToDisplay].h/2; int w1 = s1->frames[s1->frameToDisplay].w; int h1 = s1->frames[s1->frameToDisplay].h;  
	int x2 = s2->x - s2->frames[s2->frameToDisplay].w/2; int y2 = s2->y - s2->frames[s2->frameToDisplay].h/2; int w2 = s2->frames[s2->frameToDisplay].w; int h2 = s2->frames[s2->frameToDisplay].h;  
	// cout<<"checkCollision: x1 = "<<x1<<", y1 = "<<y1<<", w1 = "<<w1<<", h1 = "<<h1<<", x2 = "<<x2<<", y2 = "<<y2<<", w2 = "<<w2<<", h2 = "<<h2<<", checkCollision: outputValue = "<<outputValue<<"\n"; 
	outputValue = ((x1<x2+w2)&&(x2 < x1+w1)&&(y1 < y2+h2)&&(y2 < y1+h1));	// check if a collision occurs  
	return outputValue;  
}

void update(float deltaTimeInSec) {
	#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
		strcpy_s(msgToDisplay, "");
	#else
		strcpy(msgToDisplay, "");
	#endif
	// cout<<"update: deltaTimeInSec = " <<deltaTimeInSec<<"\n"; 
	if (in.key[SDL_SCANCODE_ESCAPE] || in.quit) {
		mustStop = true; 
	}
	// if (in.mousebuttons[SDL_BUTTON_LEFT]) {
	// 	in.mousebuttons[SDL_BUTTON_LEFT] = 0;		// fait une seule fois
	// 	strcpy(msgToDisplay,"Le bouton gauche de la souris a ete enfonce.  Position x de la souris = ");
	// 	char mouseXPosString[10];
	// 	sprintf(mouseXPosString, "%d", in.mousex); 
	// 	strcat(msgToDisplay, mouseXPosString); 
	// } 
	if(isAlive) {
		// Manage input and update x position for player
		player.timeInSecSinceFrameBegin += deltaTimeInSec;
		player.touchingDown = false; 
		if (in.key[SDL_SCANCODE_RIGHT]) { 
			player.flip = 1; 
			if(player.timeInSecSinceFrameBegin>=(player.anims[0]->periodInSec/((float)player.anims[0]->frameNumber))) { 
				player.frameToDisplay++; if(player.frameToDisplay>player.anims[0]->frameNumber) player.frameToDisplay = 1; 
				// cout<<"update: player.flip = "<<player.flip<<", player.timeInSecSinceFrameBegin = "<<player.timeInSecSinceFrameBegin<<", player.frameToDisplay = "<<player.frameToDisplay<<"\n"; 
				player.timeInSecSinceFrameBegin = 0; 
			}
			player.x += (int)(deltaTimeInSec*player.speed); 
			if(player.x + player.frames[player.frameToDisplay].w / 2 >= WINDOW_WIDTH) player.x = WINDOW_WIDTH - 1 - player.frames[player.frameToDisplay].w / 2; 
		} else if (in.key[SDL_SCANCODE_LEFT]) {
			player.flip = -1; 
			if(player.timeInSecSinceFrameBegin>=(player.anims[0]->periodInSec/((float)player.anims[0]->frameNumber))) { 
				player.frameToDisplay++; if(player.frameToDisplay>player.anims[0]->frameNumber) player.frameToDisplay = 1; 
				// cout<<"update: player.flip = "<<player.flip<<", player.timeInSecSinceFrameBegin = "<<player.timeInSecSinceFrameBegin<<", player.frameToDisplay = "<<player.frameToDisplay<<"\n"; 
				player.timeInSecSinceFrameBegin = 0; 
			}
			player.x -= (int)(deltaTimeInSec*player.speed); 
			if(player.x - player.frames[player.frameToDisplay].w / 2 < 0) player.x = player.frames[player.frameToDisplay].w / 2; 
		} else {
			player.frameToDisplay = 0; 
		}	
		if (in.key[SDL_SCANCODE_UP] && mayJump) {	// Up arrow has been pressed && mayJump is true 
			mayJump = false; 
			player.velocY = -500;	
		} 
		// Update player y position and velocity 
		player.y += player.velocY * deltaTimeInSec; player.velocY += worldYGravity * deltaTimeInSec; 
		// Check collision with platforms
		for (int i = 0; i < MAX_PLATFORM_NUMBER; i++) { 
			if(platforms[i]!=NULL) {
				if(checkCollision(&player, platforms[i])) { 
					// cout<<"update: collision detectee i = "<<i<<"\n"; 
					// cout<<"update: player.y = "<<player.y<<", player.frames[player.frameToDisplay].h/2 = "<<player.frames[player.frameToDisplay].h/2
					// 	<<", platforms[i]->y = "<<platforms[i]->y<<", platforms[i]->frames[platforms[i]->frameToDisplay].h/2 = "<<platforms[i]->frames[platforms[i]->frameToDisplay].h/2<<"\n"; 
					if(player.y-player.frames[player.frameToDisplay].h/2<platforms[i]->y - platforms[i]->frames[platforms[i]->frameToDisplay].h/2
					&& player.y+player.frames[player.frameToDisplay].h/2<platforms[i]->y+platforms[i]->frames[platforms[i]->frameToDisplay].h/2) {	// player is touching down platform 
						// cout<<"update: player is touching down, player.velocY = "<<player.velocY<<"\n"; 
						if(player.velocY > 0) {	// player is falling  
							// cout<<"update: player.velocY = "<<player.velocY<<"\n"; 
							player.y = platforms[i]->y-platforms[i]->frames[platforms[i]->frameToDisplay].h/2-player.frames[player.frameToDisplay].h/2; 
							player.velocY = 0; 
							player.touchingDown = true; 
						}
					} else {	// player is not touching down platform 
						if(player.x-player.frames[player.frameToDisplay].w/2<platforms[i]->x - platforms[i]->frames[platforms[i]->frameToDisplay].w/2) {	// player is touching left platform
							// cout<<"update: touching left: player.x = "<<player.x<<", player.frames[player.frameToDisplay].w/2 = "<<player.frames[player.frameToDisplay].w/2
							// 	<<", platforms[i]->x = "<<platforms[i]->x<<", platforms[i]->frames[platforms[i]->frameToDisplay].w/2 = "<<platforms[i]->frames[platforms[i]->frameToDisplay].w/2<<"\n"; 
							player.x = platforms[i]->x - platforms[i]->frames[platforms[i]->frameToDisplay].w/2 - player.frames[player.frameToDisplay].w/2; 
						}
						if(player.x+player.frames[player.frameToDisplay].w/2>platforms[i]->x + platforms[i]->frames[platforms[i]->frameToDisplay].w/2) {	// player is touching right platform
							// cout<<"update: touching right: player.x = "<<player.x<<", player.frames[player.frameToDisplay].w/2 = "<<player.frames[player.frameToDisplay].w/2
							// 	<<", platforms[i]->x = "<<platforms[i]->x<<", platforms[i]->frames[platforms[i]->frameToDisplay].w/2 = "<<platforms[i]->frames[platforms[i]->frameToDisplay].w/2<<"\n"; 
							player.x = platforms[i]->x + platforms[i]->frames[platforms[i]->frameToDisplay].w/2 + player.frames[player.frameToDisplay].w/2; 
						}					
					} 
					// if(player.y+player.frames[player.frameToDisplay].h/2>platforms[i]->y+platforms[i]->frames[platforms[i]->frameToDisplay].h/2) {	// player is touching up platform 
					// }
				}
			}
		} 
		if(player.touchingDown && !in.key[SDL_SCANCODE_UP]) mayJump = true;	// Must release up button while player is touching ground so that mayJump becomes true 
		// Update enemy position 
		if(!enemyDead) {
			if(enemy.x <= 263) { enemy.flip = -1; } 
			else if (enemy.x >= 573) { enemy.flip = + 1; } 
			enemy.x -= enemy.flip * (int)(deltaTimeInSec*enemy.speed); 
			// Check collision with enemy 
			if(checkCollision(&player, &enemy)) {
				if(player.y-player.frames[player.frameToDisplay].h/2<enemy.y - enemy.frames[0].h/2 
				&& player.y+player.frames[player.frameToDisplay].h/2<enemy.y+enemy.frames[0].h/2) {	// player is touching down enemy 
					// cout<<"update: player.y = "<<player.y<<", player.frames[player.frameToDisplay].h/2 = "<<player.frames[player.frameToDisplay].h/2
					// 	<<", enemy.y = "<<enemy.y<<", enemy.frames[0].h/2 = "<<enemy.frames[0].h/2<<"\n"; 
					enemyDead = true; 
					player.velocY = -135;
				} else {	// player is not touching down enemy 
					isAlive = false; 
					#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
						strcpy_s(msgToDisplay, "Game over (score ");
					#else
						strcpy(msgToDisplay, "Game over (score ");
					#endif
					char scoreStr [15];
					#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
						sprintf_s(scoreStr, "%d", score);
						strcat_s(msgToDisplay, scoreStr);
						strcat_s(msgToDisplay, ").  Press 'r' to restart");
					#else
						sprintf(scoreStr, "%d", score);
						strcat(msgToDisplay, scoreStr);
						strcat(msgToDisplay, ").  Press 'r' to restart");
					#endif
				}
			} 
		}
	} else {	// if player is dead 
		if(in.key[SDL_SCANCODE_R] ) {	// press 'r' to restart game 
			score = 0; 
			isAlive = true; 
			enemyDead = false; 
			player.x = 160; player.y = 470; player.flip = 1; 
			enemy.x = 520; enemy.y = 398; enemy.flip = 1; 
		}
	}
}

void draw(float deltaTimeInSec) {
	// cout<<"draw: deltaTime = " <<deltaTime<<"\n"; 
	if(strcmp(msgToDisplay,"")!=0) cout<<msgToDisplay<<"\n";  
	if(isAlive) {
		drawImage(backgroundImg, &bgRect, WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2); 
		drawImage(player.img, &player.frames[player.frameToDisplay], player.x, player.y, 0, player.flip, 1); 
		for(int i = 1; i < MAX_PLATFORM_NUMBER; i++) {
			if(platforms[i]!=NULL) {
				drawImage(platforms[i]->img, &platforms[i]->frames[platforms[i]->frameToDisplay], platforms[i]->x, platforms[i]->y, 0, 1, 1); 
			}
		}
		if(!enemyDead) drawImage(enemy.img, &enemy.frames[0], enemy.x, enemy.y, 0, enemy.flip, 1); 
	}
}
