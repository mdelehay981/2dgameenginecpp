#pragma once
#include <SDL.h>
#include <stdio.h>
#include <iostream>  
#include <vector> 
#include <cstring> 
#include "sprite/Sprite.h" 
#define RELATIVE_DATA_PATH "data" 
#define EXTENSION ".bmp"
#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480
using namespace std; 

class sdl_init_failed {}; 
class sdl_window_creation_failed {};
class sdl_renderer_creation_failed {}; 
class sdl_background_loading_failed {}; 
class sdl_texture_creation_failed {}; 
class sdl_sprite_creation_failed {}; 

class player {
public: 
	int x; 
	int y; 
	int speed; 
	Sprite* img; 
	player() {
		x = 100; 
		y = 350; 
		speed = 150; 
		img = NULL; 
	}
}; 

class Demo { 
	SDL_Window* pWindow;  
	SDL_Renderer* pRenderer; 
	SDL_Surface* pBgSurface; 
	SDL_Texture* pBgTexture;  
	Sprite* mySprite;
	player myPlayer;  
	vector<string> appEvent; 
	void init(); 
	// void load(); 
	// void update(int deltaTime); 
	// void draw(int deltaTime); 
public:
	~Demo(); 
	void Start1(); 
	void Start2(); 
	// int Start3(); 
	void ShowBackground(); 
	void CreateSprite(); 
	Sprite* newImage(const char* fileName, int frameNumber); 
	void DeleteSprite(); 
};
