#pragma once

#include <iostream>
#include "../tools/Tools.h"
using namespace std;

class Room { 
public: 
	int horizontalSize, verticalSize; 
	BArray<int> obstacle_map; // map of obstacles  
	Room(int hs, int vs);
}; 
