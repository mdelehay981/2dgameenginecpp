#pragma once

class GameObject
{
public:
	virtual void draw(); 
	virtual void update();
	virtual void clean();
protected:
	int m_x, m_y; 
};