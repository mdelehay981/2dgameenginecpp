#include "Sprite.h"

Sprite::Sprite(SDL_Renderer* pr, SDL_Texture* pb, int ww, int wh, int px, int py) {
	pRenderer = pr; 
	pBackground = pb; 
	windowWidth = ww;
	windowHeigth = wh;
	dest = {0, 0, 0, 0}; 
	currentFrame = 0; 
	totalFrameNbr = 0;
	frameDisplayPeriod = 0; 
	PosX = px, PosY = py; 
}

Sprite::~Sprite() {
	for (int i = 0; i < totalFrameNbr; i++) {
		if(vpFrames[i]) {
			SDL_DestroyTexture(vpFrames[i]);
		}
	}
}

void Sprite::setCurrentFrame(int frameIndex) {	
	currentFrame = frameIndex;	//printf("SetCurrentFrame: %d\n", currentFrame);
	dest.x = PosX - dest.w/2; dest.y = PosY - dest.h/2; 
	SDL_RenderCopy(pRenderer,vpFrames[currentFrame],NULL,&dest);	// Copy frame to renderer 
}

void Sprite::computePossibleStepLengths() {
	int maxAbsoluteError = (int)(theoreticalStepLength*0.1f)+1; //cout<<"Sprite::computeStepLengths(): maxAbsoluteError: "<<maxAbsoluteError<<"\n"; 
	float infRelativeError, supRelativeError;
	for(int i = theoreticalStepLength - maxAbsoluteError; i <= theoreticalStepLength + maxAbsoluteError; i++) {
		infRelativeError = (float)(theoreticalStepLength - maxAbsoluteError - i)/i;	//	inferior relative error
		supRelativeError = (float)(theoreticalStepLength + maxAbsoluteError - i)/i;	//	superior relative error
		// cout<<infRelativeError<<", "<<supRelativeError<<"\n"; 
		mPossibleStepLengths.insert(pair<int, pair<float, float>>(i,pair<float, float>(infRelativeError, supRelativeError)));
	} 
	/*#ifdef DEBUG_SPRITE
		cout<<"possible step lengths: \n"; 
		for(map<int, pair<float,float>>::const_iterator it = mPossibleStepLengths.begin(); it != mPossibleStepLengths.end(); it++) cout << it->first << " (relative error: ["<<it->second.first << ", " << it->second.second << "])\n";
	#endif*/
}

void Sprite::computeStepLengthSequenceFor(int distance, int direction) {
	float distanceByStepLengthRatio; int chosenMainStepLength = 0, mainStepLengthFactor = 0, dirSign;  
	if(direction == DIR_LEFT) dirSign = -1; 
	else if (direction == DIR_RIGHT) dirSign = 1; 
	for(map<int, pair<float,float>>::const_iterator it = mPossibleStepLengths.begin(); it != mPossibleStepLengths.end(); it++) {
		distanceByStepLengthRatio = (float)distance / (float)it->first;
		// cout << it->first << " (relative error: ["<<it->second.first << ", " << it->second.second << "]), distanceByStepLengthRatio "<<distanceByStepLengthRatio<<"\n"; 
		if(distanceByStepLengthRatio>=1) {
			if((distanceByStepLengthRatio-(int)distanceByStepLengthRatio)<=it->second.second) {	// distance by current step length ratio superior relative error is less or equal to that step length superior relative error 
				mainStepLengthFactor = (int)distanceByStepLengthRatio-1;
			} else if (((int)distanceByStepLengthRatio + 1 - distanceByStepLengthRatio)<= -it->second.first) {	// distance by current step length ratio inferior relative error is less or equal to that step length inferior relative error absolute value
				mainStepLengthFactor = (int)distanceByStepLengthRatio;
			}
			if(mainStepLengthFactor) {
				chosenMainStepLength = it->first; 
				// cout<<"chosenMainStepLength: "<<chosenMainStepLength<<"\n"; 
				break; 
			}
		} else chosenMainStepLength = it->first; mainStepLengthFactor = 0;	// if all distanceByStepLengthRatio values are < 1, take modulo 
	}
	if(chosenMainStepLength) { 
		for(int i = 0; i < mainStepLengthFactor;i++) vStepLenghtSequence.push_back(dirSign*chosenMainStepLength);
		vStepLenghtSequence.push_back(dirSign*(distance - chosenMainStepLength*mainStepLengthFactor)); 
	} else {
		// if no solution has been found, take integer division and modulo to decompose distance to go
		for(int i = 0; i < distance / theoreticalStepLength; i++) vStepLenghtSequence.push_back(dirSign*theoreticalStepLength); 
		vStepLenghtSequence.push_back(dirSign*(distance % theoreticalStepLength)); 
	}
	#ifdef DEBUG_SPRITE
		cout<<"step length sequence for distance "<<distance<<", direction "<< direction << ": \n"; 
		for(vector<int>::const_iterator it = vStepLenghtSequence.begin(); it != vStepLenghtSequence.end(); it++) cout << *it << " "; 
		cout<<"\n"; 
	#endif
}

void Sprite::LoadFrame(char* filePath) {
	SDL_Surface* pSpSurface = SDL_LoadBMP(filePath);
	SDL_SetColorKey(pSpSurface, SDL_TRUE, SDL_MapRGB(pSpSurface->format, 0, 0, 0));	// Apply transparency 
	if (pSpSurface) {
		SDL_Texture* pSpTexture = SDL_CreateTextureFromSurface(pRenderer,pSpSurface); 
		if (pSpTexture) {
			if(dest.w == 0) {
				dest.w = pSpSurface->w;
				dest.h = pSpSurface->h;
				theoreticalStepLength = 0.5 * 0.52 * dest.h;	// Average sprite step length due to its height 
				computePossibleStepLengths();	// compute possible step lengths and their relative errors 
			}
			vpFrames.push_back(pSpTexture);	
			totalFrameNbr++;
		} else fprintf(stdout,"Fail to create texture from surface (%s)\n",SDL_GetError());
		SDL_FreeSurface(pSpSurface);
	} else fprintf(stdout,"Fail to load frame in surface (%s)\n",SDL_GetError());
}

void Sprite::MoveHorizon(int distance_to_go, int dir) {
	vStepLenghtSequence.clear(); computeStepLengthSequenceFor(distance_to_go, dir); 
	if(!vStepLenghtSequence.empty()) {
		int curTime = 0, prevTime = 0; SDL_Rect prevDest; SDL_RendererFlip flip; 
		if(frameDisplayPeriod == 0) frameDisplayPeriod = 1000 * 1 / totalFrameNbr;	// If empty, calculate frame display period in ms
		prevDest.w = dest.w; prevDest.h = dest.h; prevDest.x = dest.x; prevDest.y = dest.y;	// Initialize prevDest 
		if (dir == DIR_LEFT) flip = SDL_FLIP_HORIZONTAL;	// Flip while moving  
		else if (dir == DIR_RIGHT) flip = SDL_FLIP_NONE;	// Do not flip while moving 
		#ifdef DEBUG_SPRITE
			cout << "distance_to_go: " << distance_to_go << ", dir: " << dir << ", currentFrame: "<< currentFrame <<", PosX: " << PosX << ", dest.w/2: " << dest.w/2 << ", dest.x: " << dest.x << ", prevDest.x: " << prevDest.x << "\n"; 
		#endif
		vector<int>::const_iterator it = vStepLenghtSequence.begin();
		// Increment frame index and position X in advance 
		currentFrame = ++currentFrame % totalFrameNbr;	//printf("currentFrame: %d\n", currentFrame); 
		PosX += *it;	
		dest.x = PosX - dest.w/2;	// Recalculate destination 
		while (it != vStepLenghtSequence.end()) {	// Run loop sequence if distance after it is less than total distance to go 
			curTime = SDL_GetTicks();			
			if(curTime - prevTime > frameDisplayPeriod) {	 
				prevTime = curTime;	// Save previous time 
				SDL_RenderCopy(pRenderer,pBackground,&prevDest,&prevDest);	SDL_RenderPresent(pRenderer);	// Display background instead of current frame 
				SDL_RenderCopyEx(pRenderer, vpFrames[currentFrame], NULL, &dest, 0, NULL, flip); SDL_RenderPresent(pRenderer);	// Display next frame and flip it  
				#ifdef DEBUG_SPRITE
					cout << "iteration step length (*it): " << *it << ", currentFrame: "<< currentFrame <<", PosX: " << PosX << ", dest.x: " << dest.x << ", prevDest.x: " << prevDest.x << "\n"; 
					// SDL_Delay(500);
				#endif 
				it++; 
				currentFrame = ++currentFrame % totalFrameNbr;	//printf("currentFrame: %d\n", currentFrame); 
				PosX += *it; 	
				prevDest.x = dest.x;	// Save previous dest.x	
				dest.x = PosX - dest.w/2;	// Recalculate destination
			}			
		}
		// Restore variables to previous values 
		currentFrame = --currentFrame; if((currentFrame) < 0) currentFrame = totalFrameNbr - 1;
		PosX -= *it;	
		dest.x = prevDest.x; 
	}
}

void Sprite::Show() {
	setCurrentFrame(currentFrame); 
	SDL_RenderPresent(pRenderer);
}

void Sprite::Hide() {
	SDL_RenderCopy(pRenderer,pBackground,&dest,&dest);
	SDL_RenderPresent(pRenderer);
}

void Sprite::draw() {

}

void Sprite::update() {

}

void Sprite::clean() {
	
}
