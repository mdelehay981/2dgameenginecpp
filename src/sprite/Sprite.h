#pragma once
#include <SDL.h>
#include <stdio.h>
#include <iostream> 
#include <vector> 
#include <map>
#include "GameObject.h"
using namespace std;
#define DIR_LEFT 0
#define DIR_RIGHT 1
#define DEBUG_SPRITE

class Sprite //: public GameObject
{
	SDL_Renderer* pRenderer;
	SDL_Texture* pBackground; 
	int windowWidth, windowHeigth;
	vector<SDL_Texture*> vpFrames;
	int currentFrame, totalFrameNbr;
	int frameDisplayPeriod = 0;
	int theoreticalStepLength; 
	map<int,pair<float, float>> mPossibleStepLengths;
	vector<int> vStepLenghtSequence; 
	void setCurrentFrame(int frameIndex);
	void computePossibleStepLengths(); 
	void computeStepLengthSequenceFor(int distance, int direction); 
public:
	int PosX, PosY; 
	SDL_Rect dest; 
	Sprite(SDL_Renderer* pr, SDL_Texture* pb, int ww = 640, int wh = 480, int px = 320, int py = 240); 
	~Sprite();
	void LoadFrame(char*);
	void MoveHorizon(int distance_to_go, int dir); 
	void Show();  
	void Hide();
	void draw(); 
	void update();
	void clean();
};