#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <iostream>  
#include <cstring> 
using namespace std; 
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define MAX_FRAME_PER_CHARACTER 50
#define MAX_ANIMATION_PER_CHARACTER 10
#define MAX_FRAME_PER_ANIMATION 15
#define MAX_PLATFORM_NUMBER 25
#define MIN_FRAME_PERIOD_IN_MS 16 

typedef struct {
	char key[SDL_NUM_SCANCODES];
	int mousex,mousey,mousexrel,mouseyrel;
	char mousebuttons[8];
    char quit;
} Input;

typedef struct {
	int frameNumber;   
	float periodInSec; 
	int sequence[MAX_FRAME_PER_ANIMATION]; 
} Animation;

typedef struct {
	int x, y, speed, velocY, flip, frameToDisplay;   
	float timeInSecSinceFrameBegin; 
	bool touchingDown; 
	SDL_Texture * img;
	SDL_Rect frames[MAX_FRAME_PER_CHARACTER];
	Animation * anims [MAX_ANIMATION_PER_CHARACTER];  
} Sprite;

void run(); 
void load();
void drawImage(SDL_Texture*, SDL_Rect*, int, int, int, int, int);
bool checkCollision(Sprite*, Sprite*);
void update(float deltaTime);
void draw(float deltaTime); 
void init_SDL(); 
SDL_Texture* newImage(const char*); 
void UpdateEvents(Input*); 

// exception classes 
class sdl_init_failed {}; 
class sdl_window_creation_failed {};
class sdl_renderer_creation_failed {}; 
class sdl_bitmap_loading_failed {}; 
class sdl_texture_creation_failed {}; 
class sdl_sprite_creation_failed {}; 
