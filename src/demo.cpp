#include "demo.h"

void Demo::init() {
	if (SDL_Init(SDL_INIT_VIDEO) == 0) { // Create window 
		pWindow = SDL_CreateWindow("My sprite SDL",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		WINDOW_WIDTH, WINDOW_HEIGHT,
		SDL_WINDOW_SHOWN);
		if(pWindow) { // Create renderer 
			pRenderer = SDL_CreateRenderer(pWindow,-1,SDL_RENDERER_ACCELERATED);
			if(!pRenderer) throw sdl_renderer_creation_failed();  
		} else throw sdl_window_creation_failed(); 
	} else throw sdl_init_failed(); 
	ShowBackground();	// Load and display background 
}

Demo::~Demo() {
	DeleteSprite(); 
	SDL_DestroyTexture(pBgTexture); 
	SDL_FreeSurface(pBgSurface);
	SDL_DestroyRenderer(pRenderer); 
	SDL_DestroyWindow(pWindow); 
	SDL_Quit();
}

void Demo::ShowBackground() { // Load background picture in surface 
	char path[200];
	#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
		strcpy_s(path, RELATIVE_DATA_PATH);
		strcat_s(path, "/fonds");
		strcat_s(path, EXTENSION);
	#else
		strcpy(path, RELATIVE_DATA_PATH);
		strcat(path, "/fonds");
		strcat(path, EXTENSION);
	#endif
	//printf("%s\n", path); 
	pBgSurface = SDL_LoadBMP(path);
	if(pBgSurface) { // Create texture from surface 	
		pBgTexture = SDL_CreateTextureFromSurface(pRenderer,pBgSurface); 
		if (pBgTexture) { 
			SDL_RenderCopy(pRenderer,pBgTexture,NULL,NULL); 
			SDL_RenderPresent(pRenderer);	// Display background 
		} else throw sdl_texture_creation_failed(); 
	} else throw sdl_background_loading_failed(); 
}

void Demo::CreateSprite() {
	char path[200], midPath[2]; 
	mySprite = new Sprite(pRenderer,pBgTexture,WINDOW_WIDTH,WINDOW_HEIGHT,100,350);	// Create sprite 
	if(mySprite) {
		for(int i = 0; i < 5; i++) {	
			#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
				strcpy_s(path, RELATIVE_DATA_PATH);
				strcat_s(path, "/sprite");
				sprintf_s(midPath, "%d", i + 1);
				strcat_s(path, midPath);
				strcat_s(path, EXTENSION);
			#else
				strcpy(path, RELATIVE_DATA_PATH);
				strcat(path, "/sprite");
				sprintf(midPath, "%d", i + 1);
				strcat(path, midPath);
				strcat(path, EXTENSION);
			#endif
			//printf("%s\n", path); 
			mySprite->LoadFrame(path);
		} 
	} else throw sdl_sprite_creation_failed(); 
}

Sprite* Demo::newImage(const char* fileName, int frameNumber) {
	char path[200], midPath[2]; 
	mySprite = new Sprite(pRenderer,pBgTexture,WINDOW_WIDTH,WINDOW_HEIGHT,0,0);	// Create sprite 
	if(mySprite) {
		for(int i = 0; i < frameNumber; i++) {	
			#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
				strcpy_s(path, RELATIVE_DATA_PATH);
				strcat_s(path, "/");
				strcat_s(path, fileName);
				sprintf_s(midPath, "%d", i + 1);
				strcat_s(path, midPath);
				strcat_s(path, EXTENSION);
			#else
				strcpy(path, RELATIVE_DATA_PATH);
				strcat(path, "/");
				strcat(path, fileName);
				sprintf(midPath, "%d", i + 1);
				strcat(path, midPath);
				strcat(path, EXTENSION);
			#endif
			//printf("%s\n", path); 
			mySprite->LoadFrame(path);
			return mySprite; 
		} 
	} else throw sdl_sprite_creation_failed(); 
}

void Demo::DeleteSprite() { delete mySprite; } 

void Demo::Start1() {
	init(); CreateSprite();	 
	mySprite->Show();	
	SDL_Delay(500);
	mySprite->MoveHorizon(450, DIR_RIGHT);
	SDL_Delay(500);
	mySprite->MoveHorizon(480, DIR_LEFT);	
	SDL_Delay(1500);
}

void Demo::Start2() {
	init(); CreateSprite();	 
	int state = 0, mouseX = 0, distance = 0, dir; bool mustStop = false; SDL_Event event;  
	mySprite->Show();	
	while(!mustStop) {
		switch(state) {
			case 0:	// wait for mouse click  
				if(SDL_PollEvent(&event)&&event.type==SDL_MOUSEBUTTONDOWN) {
					if(event.button.button == SDL_BUTTON_RIGHT) mustStop = true; 
					else SDL_GetMouseState(&mouseX, NULL); state = 1;	
				}
				break;
			case 1:	// wait for mouse button release 
				if(SDL_PollEvent(&event)&&event.type==SDL_MOUSEBUTTONUP) state = 2; 
				break;
			case 2:	// move sprite 
				#ifdef DEBUG_SPRITE
					cout<<"\nmouseX "<<mouseX; 
				#endif	
				if(mouseX > mySprite->PosX + mySprite->dest.w/2) {	// player clicked to the right of character  
					if(mouseX >= WINDOW_WIDTH - mySprite->dest.w/2) distance = WINDOW_WIDTH - mySprite->dest.w/2 - mySprite->PosX; 
					else distance = mouseX - mySprite->PosX; 
					dir = DIR_RIGHT;
				} else if(mouseX < mySprite->PosX - mySprite->dest.w/2) {	// player clicked to the left of character 
					if(mouseX <= mySprite->dest.w/2) distance = mySprite->PosX - mySprite->dest.w/2; 
					else distance = mySprite->PosX - mouseX; 
					dir = DIR_LEFT;   
				}
				if(distance) { 
					#ifdef DEBUG_SPRITE
						cout<<", distance "<<distance<<", dir "<<dir<<"\n"; 
					#endif	
					mySprite->MoveHorizon(distance, dir); 
				}
				SDL_PumpEvents(); SDL_FlushEvent(SDL_MOUSEBUTTONDOWN);	// pump pending events to the queue and flush it from SDL_MOUSEBUTTONDOWN events 
				distance = 0; state = 0; 
				break; 
		}		
	}
}
