#pragma once

#include <iostream>
#include <string>
#include "../character/Character.h"
#include "../tools/Tools.h"

using namespace std;

class Battle { 
public: 
	static int resolve(Character * attacker, Character * defender);
}; 
