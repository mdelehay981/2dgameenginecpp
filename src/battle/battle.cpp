#include "battle.h"

int Battle::resolve(Character * attacker, Character * defender) {
	int totalAttack = 0, totalDefense = 0; 
	int diceResults[Tools::maxDiceNumber]; 
	for(int i = 0; i < Tools::maxDiceNumber; i++) {
		diceResults[i] = 0;
	}
	Tools::throwDices(attacker->getAP()+defender->getDP(), 6, diceResults);  
	cout << "\nJet(s) de de(s) du " << attacker->type << ": "; 
	for(int i = 0; i<attacker->getAP();i++) {
		totalAttack += diceResults[i]; 
		cout<<diceResults[i]; 
		if(i != attacker->getAP()-1) {
			cout<<", "; 
		}
	}
	cout <<"\n"; 
	cout << "\nJet(s) de de(s) du " << defender->type << ": "; 
	for(int i = 0; i<defender->getDP();i++) {
		totalDefense += diceResults[attacker->getAP()+i]; 
		cout<<diceResults[attacker->getAP()+i]; 
		if(i != defender->getDP()-1) {
			cout<<", "; 
		}
	}
	cout <<"\n"; 
	cout << "\nAttaque totale: " << totalAttack << ", " << "defense totale: " << totalDefense << ".\n"; 
	if (totalAttack > totalDefense) { 
		cout << "\nLe " << attacker->type << " inflige 1 point de degat au " << defender->type << ".\n"; 
		defender->incrementHPBy(-1); 
	}
	else
	{
		cout << "\nLe " << attacker->type << " manque le " << defender->type << ".\n"; 
	}
	if(defender->getHP() <= 0) { 
		cout << "\nLe " << attacker->type << " tue le " << defender->type << ".\n"; 
		return 0; 
	} 
	else { 
		cout << "\nLe " << defender->type << " survit.\n"; 
		return 1; 
	} 
}
