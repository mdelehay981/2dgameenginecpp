#include "Randomgen.h"

Urand::Urand(int nn) { 
	n = nn; 
}

#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
	int Urand::operator()() { 
		int r = n * fdraw(); 
		return (r == n) ? n - 1 : r; 
	}
#else
	Urand::operator()() { 
		int r = n * fdraw(); 
		return (r == n) ? n - 1 : r; 
	}
#endif
