#include "Character.h"

Enemy::Enemy(int px, int py, int ap, int dp, int hp, int md) : Character(px, py, ap, dp, hp) {
	type = "monstre"; 
	moveDistance = md;
}

#ifdef _MSC_VER	//	detect is Visual Studio is used as compiler 
	int Enemy::getMD() { return moveDistance; }
#else
	Enemy::getMD() { return moveDistance; }
#endif
