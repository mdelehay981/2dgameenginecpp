#include "gtest/gtest.h"
#include "tiledjson.hpp"

namespace {

	// The fixture for testing class.  
	class tiledjsonTest : public ::testing::Test {
	protected:
		
		tiledjsonTest() {
			// You can do set-up work for each test here.
		}

		~tiledjsonTest() override {
			// You can do clean-up work that doesn't throw exceptions here.
		}

		// If the constructor and destructor are not enough for setting up
		// and cleaning up each test, you can define the following methods:

		void SetUp() override {
			// Code here will be called immediately after the constructor (right
			// before each test).
		}

		void TearDown() override {
			// Code here will be called immediately after each test (right
			// before the destructor).
		}

		// Objects declared here can be used by all tests in the test case.
	};

	// Test cases 
	TEST_F(tiledjsonTest, MethodThrowDicesSucceeds2) {
		// Create TiledMap object
		tiledjson::TiledMap theMap;
		// Tell it to load a map from a file.
		theMap.loadFromFile("../data/level_map.json");
		
		EXPECT_EQ(1, 1);
	}

}  // namespace
