#include "tools.h"

// Throw many dices with faceNumber faces and fill out results into an array 
void Tools::throwDices(int diceNumber, int faceNumber, int* diceResults) {
	Urand draw(6);
	for(int i=0; i< 10000000; i++);	// wait for several cycles before reading clock value  
	clock_t t = clock(); 
	if(t == clock_t(-1)) {
		cerr << "Tools::throwDices: no clock found\n";
		throw Tools_td_no_clock();
	}
	for (int i = 0; i < t; i++) { draw(); }
	if(diceNumber>maxDiceNumber) {
		cerr <<"Tools::throwDices: no more than "<<maxDiceNumber<<" dices allowed\n"; 
		throw Tools_td_two_many_dices(diceNumber);
	} else {
		for (int i = 0; i < diceNumber; i++) {
			diceResults[i] = draw()+1;
		}
	}
}

// Wait for player's keystroke 
void Tools::waitForAKey() {
	int i; 
	fflush(stdin); 
	while((i = getchar()) != EOF) { break; }
}

// Read a string on keyboard  
string Tools::readAString() {
	int i; 	
	string outString = ""; 
	fflush(stdin); 
	while((i = getchar()) != EOF) { 		
		if(i == '\n') { break; }
		else { outString += char(i); }
	}
	return outString; 
}

// Convert a string to an int 
int Tools::CStringToInt(char* CString) {
	int result = 0; 
	int radixToPowerOfExponent = 1; for(int i = 0; i < strlen(CString) - 1; i++) radixToPowerOfExponent *= 10; 
	bool negativeOutput = false; 
	for(int i = 0; i < strlen(CString); i++) {
		if ((CString[i] < 48 || CString[i]>57) && CString[i] != 45) {
			throw Tools_csti_illegal_char(CString);
		} 
		else {
			if (CString[i] == 45) {
				// '-'
				negativeOutput = true;
				radixToPowerOfExponent /= 10;	//	exponent must be devided by 10  
			} 
			else {
				// ['0', '9']
				result += radixToPowerOfExponent * (CString[i] - 48);
				radixToPowerOfExponent /= 10;
			}
		}
	}
	if (negativeOutput) {
		result = -result; 
	}
	return result; 
}

// Exception class constructor 
Tools_td_two_many_dices::Tools_td_two_many_dices(int i) {
	this->askedNumber = i;
}

Tools_csti_illegal_char::Tools_csti_illegal_char(char* s) {
	this->inputString = s; 
}
