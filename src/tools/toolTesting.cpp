#include "tools.h"
#include "gtest/gtest.h"

namespace {

	// The fixture for testing class.  
	class toolTest : public ::testing::Test {
	protected:
		
		toolTest() {
			// You can do set-up work for each test here.
		}

		~toolTest() override {
			// You can do clean-up work that doesn't throw exceptions here.
		}

		// If the constructor and destructor are not enough for setting up
		// and cleaning up each test, you can define the following methods:

		void SetUp() override {
			// Code here will be called immediately after the constructor (right
			// before each test).
		}

		void TearDown() override {
			// Code here will be called immediately after each test (right
			// before the destructor).
		}

		// Objects declared here can be used by all tests in the test case.
	};

	// Test cases 
	TEST_F(toolTest, MethodThrowDicesSucceeds) {
		int diceNumber = 5;
		int faceNumber = 6; 
		int diceResults[Tools::maxDiceNumber];
		for (int i = 0; i < Tools::maxDiceNumber; i++) {
			diceResults[i] = 0;	// initialize dice results to 0  
		}
		Tools::throwDices(diceNumber, faceNumber, diceResults);
		for (int i = 0; i < diceNumber; i++) {
			EXPECT_GT(diceResults[i], 0);	// check that dices are not null  
			EXPECT_LE(diceResults[i], faceNumber);	// check that dices are not gt face number 
		}
		for (int i = diceNumber; i < Tools::maxDiceNumber; i++) {
			EXPECT_EQ(diceResults[i], 0);
		}
	}

	TEST_F(toolTest, MethodThrowDicesFailsWithTooManyDices) {
		int diceNumber = 11;	// diceNumber > Tools::maxDiceNumber !!! 
		int faceNumber = 6;
		int diceResults[Tools::maxDiceNumber];
		for (int i = 0; i < Tools::maxDiceNumber; i++) {
			diceResults[i] = 0;	// initialize dice results to 0  
		}
		try {
			Tools::throwDices(diceNumber, faceNumber, diceResults);
		}
		catch (Tools_td_two_many_dices e) {
			EXPECT_GT(e.askedNumber, Tools::maxDiceNumber);	// after exception, check out input value 
		}
	}

	TEST_F(toolTest, MethodCStringToIntSucceedsWithNatural) {
		char inputString[10] = {'1', '0', '\0' };
		int outputInt = Tools::CStringToInt(inputString); 
		EXPECT_EQ(outputInt, 10);
	}

	TEST_F(toolTest, MethodCStringToIntSucceedsWithNegativeInteger) {
		char inputString[10] = { '-', '1', '5', '\0' };
		int outputInt = Tools::CStringToInt(inputString);
		EXPECT_EQ(outputInt, -15);
	}

	TEST_F(toolTest, MethodCStringToIntFailsWithDecimal) {
		char inputString[10] = { '2', '.', '7', '\0' };
		int outputInt = 0;
		try {
			outputInt = Tools::CStringToInt(inputString); 
		}
		catch (Tools_csti_illegal_char e) {
			EXPECT_STREQ(e.inputString, "2.7");	// after exception, check out input value 
		}
	}

}  // namespace
